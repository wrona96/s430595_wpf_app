﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WPF_MVVM.Models;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.IO;

namespace WPF_MVVM.ViewModels
{
    public class TodosVM
    {
        public ObservableCollection<Todo> Todos { get; set; }
        public TodosVM()
        {
            Todos = new ObservableCollection<Todo>();
        }

        public void AddTodo(string name, string numberString)
        {
            Todos.Add(new Todo("title", "qual" ,"url"));
        }

        public async void ExportJson(string path)
        {
            if (!Regex.Match(path, @"\.json$").Success)
            {
                path = path + ".json";
            }
            string result = JsonConvert.SerializeObject(Todos);
            await Task.Run(() => { WriteToFile(path, result); });
        }
        public async void ExportM3U(string path)
        {
            if (!Regex.Match(path, @"\.m3u$").Success)
            {
                path = path + ".m3u";
            }
            string result = "#EXTM3U\n";
            string pattern = "#EXTINF:-1 tvg-logo=\"\" ,";
            foreach (Todo T in Todos)
            {
                result = result + pattern + T.Title + "(" + T.Quality + ")\n" + T.URL + "\n";

            }
            await Task.Run(() => { WriteToFile(path, result); });
            
        }

        public async void Import(string data)
        {
            Todo[] imported = JsonConvert.DeserializeObject<Todo[]>(data);
            if (imported == null)
            {
                return;
            }
            
            foreach (Todo import in imported)
            {
               await App.Current.Dispatcher.Invoke( async() =>
                {
                    Todos.Add(import);
                });
                
            }
            
        }
        private void WriteToFile(string path, object data)
        {
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter(path, false);
                writer.Write(data);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }
        internal void DeleteTodo(Todo selectedItem)
        {
            if (Todos.Contains(selectedItem))
            {
                Todos.Remove(selectedItem);
            }
        }

       public void AddRawFilm(string title, string qual,string url)
        {
            Todos.Add(new Todo(title, qual, url));
        }
        public async Task AddTodoFromWebAsync(string id)
        {
            using (HttpClient client = new HttpClient())
            {
                string[] qual = { @"1080p", @"720p", @"480p", @"320p" };
                string quality = "default";
                // Call asynchronous network methods in a try/catch block to handle exceptions
                try
                {

                    HttpResponseMessage test = await client.GetAsync(" https://ebd.cda.pl/1440x900/" + id);
                    if (test.StatusCode.ToString() == "NotFound")
                    {
                        return;
                    }

                    string cda = await test.Content.ReadAsStringAsync();
                    if (Regex.Match(cda, @"premiumInfo").Success)
                    {
                        return;
                    }

                    foreach (string q in qual)
                    {
                        if (Regex.Match(cda, q).Success)
                        {
                            test = await client.GetAsync(" https://ebd.cda.pl/1440x900/" + id + "?wersja=" + q);
                            cda = await test.Content.ReadAsStringAsync();
                            quality = q;
                            break;
                        }
                    }
                    
                    Trace.WriteLine(quality);
                    Match title = Regex.Match(cda, @"<title>(.*)</title>");
                    string parsed_title = title.Groups[1].Value;
                    
                    Match url = Regex.Match(cda, @"file.:.(https:.*mp4)");
                    string parsed_url = (url.Groups[1].Value).Replace("\\", "");

                    Todos.Add(new Todo(parsed_title, quality, parsed_url));
                }
                catch (HttpRequestException e)
                {
                }
            }
        }
        
    }
}
