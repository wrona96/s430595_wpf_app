﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_MVVM.Models;
using WPF_MVVM.ViewModels;

namespace WPF_MVVM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public TodosVM vm { get { return DataContext as TodosVM; }}
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string id = InputTodoName.Text;
            InputTodoName.Text = "";
            await vm.AddTodoFromWebAsync(id);
        }
        private void RawAddButton_Click(object sender, RoutedEventArgs e)
        {
            vm.AddRawFilm(RawInputTitle.Text, ((ComboBoxItem)RawQuality.SelectedItem).Content.ToString() ,RawInputURL.Text);
            RawInputTitle.Text = "";
            RawInputURL.Text = "";
            RawQuality.Text = "default";
        }
        private void JSONButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Json files (*.json)|*.json";
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.DefaultExt = "*.json";
            if (saveFileDialog.ShowDialog() == true)
            {
                vm.ExportJson(saveFileDialog.FileName);
            }
           
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            vm.DeleteTodo((Todo)(TodosListView.SelectedItem));
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            TodosListView.UnselectAll();
        }
        
        private void CopySelected_Click(object sender, RoutedEventArgs e)
        {
            Todo selected = (Todo)(TodosListView.SelectedItem);
            if (selected != null) { Clipboard.SetText(selected.URL); TodosListView.UnselectAll(); }
            
        }
        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Json files (*.json)|*.json";
            if (openFileDialog.ShowDialog() == true) {
                vm.Import(File.ReadAllText(openFileDialog.FileName));
            }
                
        }
        private void M3UButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Json files (*.m3u)|*.m3u";
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.DefaultExt = "*.m3u";
            if (saveFileDialog.ShowDialog() == true)
            {
                vm.ExportM3U(saveFileDialog.FileName);
            }
    
        }
    }
}
