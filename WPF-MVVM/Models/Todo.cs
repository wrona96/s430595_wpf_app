﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_MVVM.Models
{
    public class Todo : INotifyPropertyChanged
    {
        //propf tab tab=> skrót do propfull
        private string title;

        public string Title
        {
            get { return title; }
            set { title = value; PropertyChange(nameof(Title)); }
        }

        private string url;

        public string URL
        {
            get { return url; }
            set { url = value; PropertyChange(nameof(URL)); }
        }

        private string quality;

        public string Quality
        {
            get { return quality; }
            set { quality = value; PropertyChange(nameof(Quality)); }
        }
        public Todo(string title, string qual ,string url)
        {
            Title = title;
            URL = url;
            Quality = qual;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        private void PropertyChange(string propertyName)
        {
            if (PropertyChanged!=null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
